package com.example.assignment_04_weather_app;



import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insertrecord(User  users);

    @Query("SELECT * FROM User" )
    List<User> getalldata();

}