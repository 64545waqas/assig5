package com.example.assignment_04_weather_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class DisplayData extends AppCompatActivity {
    Button fetch;
    TextView data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_data);
        fetch = (Button)findViewById(R.id.fetch_btn);
        data = (TextView) findViewById(R.id.dataholder);

     fetch.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "databasenew").allowMainThreadQueries().build();
            UserDao userDao = db.userDao();

            List<User> users = userDao.getalldata();
            String str ="";

            for (User user : users)
                str=str+"\t"+user.getUid()+" "+user.getCountry()+" "+user.getCity()+" "+user.getTemprature()+" "+user.getSpeed()+" "+user.getHumidity()+"\n";
            data.setText(str);



        }
    });
    }
}